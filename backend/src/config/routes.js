const express = require('express');



//Exporta a função com um parâmetro definido ao utilizar require('./routes')
module.exports = function(server) {
    const router = express.Router();

    //URL Base
    server.use('/api', router);

    //Rotas de Ciclo de Pagamentos com os métodos padrões
    const BillingCycle = require('../api/billingCycle/billingCycleService');
    BillingCycle.register(router, '/billingCycles')
}