const constants = require('./constants')

const mongoose = require('mongoose')
mongoose.Promise = global.Promise 
module.exports = mongoose.connect(constants.URL_DB, { useNewUrlParser: true })

mongoose.connection.on('connected', () => { console.log('database connected') } );
mongoose.connection.on('error', () => { console.log('database error') } );
