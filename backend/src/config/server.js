const constants = require('./constants')
const bodyParser = require('body-parser')
const express = require('express')
const allowCors = require('./cors')
const queryParser = require('express-query-int')

//Instanciando o servidor
const server = express()

//Ativando os Middlewares para todas as requisições
server.use(bodyParser.urlencoded({ extended: true }))
server.use(bodyParser.json())
server.use(allowCors)
server.use(queryParser())

server.listen(constants.PORT, function(){
    console.log('Backend is running on Port '+constants.PORT);
});

module.exports = server;