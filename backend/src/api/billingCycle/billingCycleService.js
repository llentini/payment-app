const BillingCycle = require('./billingCycle');
const errorHandler = require('../../common/errorHandler');

//Tipos de requisições/métodos que serão aceitas
//
BillingCycle.methods(['get','post','put','delete']);

//Retornar o objeto atualizado depois de salvar as mudanças e definir que as validações devem ser verificadas também no PUT
BillingCycle.updateOptions({new: true, runValidators: true})

//Aplicando o Middleware de erros nos métodos que são criados de forma padrão pelo mongoose
BillingCycle.after('post', errorHandler)
BillingCycle.after('put', errorHandler)

/*
 * Todas as rotas criadas neste arquivo já serão mapeadas no config/routes.js com o comando "BillingCycle.register"
 * ficando a rota base 'api/billingCycles/{nova_rota}'
 * /api/billingCycles?skip=0&limit=3
 */

//Método-Rota para calcular a quantidade de ciclos de pagamento.
BillingCycle.route('count', (req, res, next) => {
    //Método já existente
    BillingCycle.count((error, value) => {
        if (error) {
            res.status(500).json({errors: [error]});
        } else {
            res.json({value});
        }
    })
});

//Método-Rota para calcular o valor geral da soma de todos os créditos e débitos cadastrados no banco.
BillingCycle.route('summary', (req, res, next) => {
    //Agregação retornando dois "campos" a partir da soma dos credits e debts
    BillingCycle.aggregate([{
        $project: {credit_sum: {$sum: "$credits.value"}, debt_sum: {$sum: "$debts.value"}}
    }, {
        //Group By para fazer a junção e realizar a soma dos creditos e debitos agregados no passo anterior, sem definir _id
        $group: {_id: null, credit_final: {$sum: "$credit_sum"}, debt_final: {$sum: "$debt_sum"}}
    }, {
        //Definindo quais campos serão retornados (0 ou 1), esse passo não é obrigatório, apenas para customizar a resposta
        $project: {_id: 0, credit_final: 1, debt_final: 1}
    }]).exec((error, result) => {
        if (error) {
            res.status(500).json({errors: [error]});
        } else {
            res.json(result[0] || {credit_final: 0, debt_final: 0});
        }
    })
});

//Método-Rota que retorna o último pagamento inserido no banco
BillingCycle.route('last', (req, res, next) => {

    BillingCycle.findOne().sort({ field: 'asc', _id: -1 }).exec(function(err, post) {
        res.json(post);
    });

    /*BillingCycle.findOne({}, {}, { sort: { field: 'asc'.  _id: -1 } }, function(err, post) {
        res.json({post});
    });*/

});


module.exports = BillingCycle;
